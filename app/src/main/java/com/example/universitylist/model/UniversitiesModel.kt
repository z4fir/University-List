package com.example.universitylist.model

data class UniversityList(val items: ArrayList<UniversitiesModel>)
data class UniversitiesModel(
    val image_url: String,
    val name: String,
    val web_pages: List<String>
)