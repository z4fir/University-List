package com.example.universitylist.model

import android.app.Activity
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.universitylist.adaptor.RecyclerviewAdaptor
import com.example.universitylist.network.ApiRequest
import com.example.universitylist.network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UniversitiesViewModel : ViewModel() {

    lateinit var universitiesData: MutableLiveData<UniversityList>
    lateinit var recyclerviewAdapter: RecyclerviewAdaptor

    init {
        universitiesData = MutableLiveData()
        recyclerviewAdapter = RecyclerviewAdaptor()
    }

    fun getAdaptor(): RecyclerviewAdaptor {
        return recyclerviewAdapter
    }

    fun setAdaptorData(data: ArrayList<UniversitiesModel>) {
        recyclerviewAdapter.setDataList(data)
        recyclerviewAdapter.notifyDataSetChanged()
    }

    fun getRecListObserver(): MutableLiveData<UniversityList> {
        return universitiesData
    }

    fun apiCall() {
        val retroInstance = ApiRequest.getRetroInstance().create(ApiService::class.java)
        val apiCall = retroInstance.getUniversities()
        apiCall.enqueue(object : Callback<UniversityList> {
            override fun onResponse(call: Call<UniversityList>, response: Response<UniversityList>) {

                universitiesData.postValue(response.body())
            }

            override fun onFailure(call: Call<UniversityList>, t: Throwable) {
                universitiesData.postValue(null)
            }
        })
    }
}