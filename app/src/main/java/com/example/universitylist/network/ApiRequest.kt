package com.example.universitylist.network

import com.example.universitylist.model.UniversitiesModel
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET



interface ApiRequest {

    companion object {
        const val BASE_URL = "http://3.128.30.248:3000"

        fun getRetroInstance(): Retrofit {
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
        }

    }

}

