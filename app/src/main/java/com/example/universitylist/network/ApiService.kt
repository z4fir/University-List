package com.example.universitylist.network

import com.example.universitylist.model.UniversitiesModel
import com.example.universitylist.model.UniversityList
import retrofit2.Call
import retrofit2.http.GET

interface ApiService{

    @GET("/data/universities")
    fun getUniversities(): Call<UniversityList>

}