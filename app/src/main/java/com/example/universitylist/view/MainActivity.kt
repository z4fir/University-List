package com.example.universitylist.view

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.universitylist.R
import com.example.universitylist.databinding.ActivityMainBinding
import com.example.universitylist.model.UniversitiesViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.rec_view
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel = doApiCall()
        setupBinding(viewModel)
    }

    private fun setupBinding(viewModel: UniversitiesViewModel) {

        val mainActivity: ActivityMainBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        mainActivity.setVariable(BR.viewModel, viewModel)
        mainActivity.executePendingBindings()

        rec_view.apply {
            LinearLayoutManager(this@MainActivity)
            onScrollStateChanged()
        }
    }

    private fun RecyclerView.onScrollStateChanged() {
        if (rec_view.canScrollVertically(1)) {
            Toast.makeText(this@MainActivity, "You've reached the end!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun doApiCall(): UniversitiesViewModel {
        val viewModel = ViewModelProvider(this).get(UniversitiesViewModel::class.java)
        viewModel.getRecListObserver().observe(this, {
            viewModel.setAdaptorData(it.items)
        })
        viewModel.apiCall()
        return viewModel
    }


}

