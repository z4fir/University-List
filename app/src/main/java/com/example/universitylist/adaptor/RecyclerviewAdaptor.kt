package com.example.universitylist.adaptor

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.universitylist.R
import com.example.universitylist.databinding.RecyclerViewBinding
import com.example.universitylist.model.UniversitiesModel

class RecyclerviewAdaptor : RecyclerView.Adapter<RecyclerviewAdaptor.MyViewHolder>() {

    private val data: List<UniversitiesModel>
        get() {
            TODO()
        }

    var items = ArrayList<UniversitiesModel>()

    fun setDataList(data: ArrayList<UniversitiesModel>) {
        this.items = data
    }

    class MyViewHolder(val binding: RecyclerViewBinding) : RecyclerView.ViewHolder(binding.root) {


        fun bind(data: UniversitiesModel) {
            binding.recyclerviewData = data
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RecyclerViewBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    companion object {
        @JvmStatic
        @BindingAdapter("loadImage")
        fun loadImage(thumbnail: ImageView, url: String) {
            Glide.with(thumbnail)
                .load(url)
                .into(thumbnail)
        }

    }
}